/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author crazm_000
 */
public class Framer {
    private String frameMaterial;
    
    public Framer (String frameMaterial){
        this.frameMaterial=frameMaterial;
    }

    public void frameText(String textToFrame) {
        System.out.println("Printing: ");
        int length = textToFrame.length();
        for (int i = 0; i < length + 2; i++) {
            System.out.print(frameMaterial);
        }
        System.out.println("");
        System.out.println(frameMaterial + textToFrame + frameMaterial);
        for (int i = 0; i < length + 2; i++) {
            System.out.print(frameMaterial);
        }
        System.out.println("");
    }
    
}
