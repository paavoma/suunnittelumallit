/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author crazm_000
 */
public class FramerAdapter extends Printer {
    public Framer framer;
    
    public FramerAdapter (Framer frame){
        this.framer=frame;
    }
    public void printText(String textToPrint){
        this.framer.frameText(textToPrint);
    }
    
}
