/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adapter;

/**
 *
 * @author crazm_000
 */
public class Printer {

    private String textToPrint;
    
    public Printer(){
        
    }

    public Printer(String textToPrint) {
        this.textToPrint = textToPrint;
    }

    public void printText(String textToPrint) {
        System.out.println("Printing:");
        System.out.println(textToPrint);
        System.out.println("");
    }

    public String getText() {
        return textToPrint;
    }

    public void setText(String textToPrint) {
        this.textToPrint = textToPrint;
    }

}
