package adapter;





public class Main{
    

  public static void main(String args[]){

    Printer printer = new Printer();
    printer.printText("without framing");
    
    printer = new FramerAdapter(new Framer("o"));
    printer.printText("with framing");
  }
  
}