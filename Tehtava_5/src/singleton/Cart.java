/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

import java.util.ArrayList;

/**
 *
 * @author crazm_000
 */
public class Cart {

    ArrayList<Product> cartContent = new ArrayList<Product>();
    OneTimeDiscount otd = null;
    double totalPrice;

    public Cart() {

    }

    public void addProduct(Product p) {
        cartContent.add(p);
        updateTotalPrice();
    }

    public void removeProduct(Product p) {
        cartContent.remove(p);
    }

    public ArrayList getCartContent() {
        return cartContent;
    }

    public Cart getCart() {
        return this;
    }

    public void printCart() {
        for (Product p : cartContent) {
            p.printProduct();
        }
    }

    public void updateTotalPrice() {
        totalPrice = getCartPrice();
    }
    
    //Korin hinta
    public double getCartPrice() {
        totalPrice = 0;
        for (Product p : cartContent) {
            totalPrice = totalPrice + p.getPrice();
        }
        if (OneTimeDiscount.isDiscountEnabled()) {
            totalPrice = totalPrice * OneTimeDiscount.getDiscount();
        }
        return totalPrice;

    }
    public void printCartPrice() {
        System.out.println("---------------------------------");
        printCart();
        String price = "Cart total price: " + getCartPrice() + "(-" + (OneTimeDiscount.getDiscount()*100) + "%)";
        System.out.println("---------------------------------");
        System.out.println(price);
    }

    // Tällä metodilla luodaan alennuskuponki (singleton). Voi olla vain yksi alennuskuponki,
    // joka on voimassa istunnon loppuun asti
    public void applyOneTimeDiscount(double discountPercentage) {
        
        otd = OneTimeDiscount.getInstance(discountPercentage);
        
    }
    
    
    

}
