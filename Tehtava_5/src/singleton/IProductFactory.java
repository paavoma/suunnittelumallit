/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author crazm_000
 */
public interface IProductFactory {
    
    public abstract IProduct createDvd();
    public abstract IProduct createTicket();
    public abstract IProduct createTshirt();
    
}
