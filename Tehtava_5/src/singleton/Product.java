/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author crazm_000
 */
public abstract class Product implements IProduct {
    double price;
    String name;
    
    public Product(){
        
    }
    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public String getName() {
       
        return name;
    }

    @Override
    public void printProduct() {
        System.out.println("Price: " + getPrice() + "   Name: " + getName());
    }
    
}
