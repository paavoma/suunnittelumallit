/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author crazm_000
 */
public class Main {

    //Demon ideana on käyttää singletonia antamaan verkkokaupan ostoskoriin 
    //kertakäyttöisen alennuksen joka on voimassa istunnon ajan. Alentaa
    //koko ostostoskorin hintaa
    public static void main(String[] args) {
        
        ProductFactory factory = new ProductFactory();
        Cart cart = new Cart();
        cart.addProduct((Product) factory.createDvd());
        cart.addProduct((Product) factory.createTshirt());
        cart.addProduct((Product) factory.createTicket());
        
        cart.printCartPrice();
        //annetaan 50% alennus
        cart.applyOneTimeDiscount(0.5);
        cart.printCartPrice();
        //annnetaan 10% alennus, joka ei pitäisi toimia
        cart.applyOneTimeDiscount(0.9);
        cart.printCartPrice();
        
        
    }
    
}
