/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author crazm_000
 */
public class ProductFactory implements IProductFactory{
    
public ProductFactory(){
    
}    

    @Override
    public IProduct createDvd() {
        return new Dvd();
    }

    @Override
    public IProduct createTicket() {
        return new Ticket();
    }

    @Override
    public IProduct createTshirt() {
        return new Tshirt();
    }
    
}
