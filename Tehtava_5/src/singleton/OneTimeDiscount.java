/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author crazm_000
 */
public class OneTimeDiscount {

    private static double discount = 0;

    //saa konstruktoriin parametrina alennusprosentin.
    private OneTimeDiscount(double discount) {
        this.discount = discount;
    }

    private static OneTimeDiscount INSTANCE = null;

    public static OneTimeDiscount getInstance(double discount) {
        if (INSTANCE == null) {
            System.out.println("You have applied a " + (100 - (discount * 100)) + "% discount coupon");
            INSTANCE = new OneTimeDiscount(discount);
        } else {
            System.out.println("A coupon with " + (100 - (getDiscount() * 100)) + "% discount already applied");
        }
        return INSTANCE;

    }

    protected static double getDiscount() {
        return discount;
    }

    protected static boolean isDiscountEnabled() {
        return discount > 0;

    }
}
