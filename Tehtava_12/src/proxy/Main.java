/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] arguments) {
        
        Folder folder = new Folder();
        folder.addImage("HiRes_10MB_Photo1", "Koira");
        folder.addImage("HiRes_10MB_Photo2", "Koira ja kissa");
        folder.addImage("HiRes_10MB_Photo3", "Perhe ja koira");
        folder.addImage("HiRes_10MB_Photo4", "Koira ja koira");
        
      
        folder.showAllNames();  //näyttää koko kansion tiedoston pelkät nimet
        folder.showCurrentImage();
        folder.showPrevImage();
        folder.showNextImage();
        folder.showNextImage();
        folder.showNextImage();
        folder.showNextImage();
        folder.showPrevImage();
        

        
        
       
        
         
    }

}
