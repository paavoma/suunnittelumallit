/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author crazm_000
 */

//Kansion sisällön hallinta
public class Folder {

    private List<Image> fold = new ArrayList<Image>();
    private Image currentImage;
    private int index;
    

    public Folder() {
        index=0;
    }

    public void addImage(String filename, String name) {
        fold.add(new ProxyImage(filename, name));
        
        currentImage=fold.get(0);
    }

    public void showAllNames() {
        
        int i = 1;
        System.out.println("Folder contains: ");
        for (Image image : fold) {

            System.out.println("File " + i + ": ");
            image.showData();
            i++;
        }
        System.out.println("------------------");
    }
    
    public void showCurrentImage(){
        int i=index+1;
        System.out.println("Displaying File " + i + ": ");
        currentImage.displayImage();
        currentImage.showData();
        System.out.println("-------------------");
    }
    
    public void showNextImage(){
       index++;
       if(index > fold.size()-1){
           index--;
           currentImage=fold.get(index);
           showCurrentImage();
       }else{
           currentImage=fold.get(index);
           showCurrentImage();
       }
    }
    
    public void showPrevImage(){
       index--;
       if(index < 0){
           index++;
           currentImage=fold.get(index);
           showCurrentImage();
       }else{
           currentImage=fold.get(index);
           showCurrentImage();
       }
    }

    
  
}
