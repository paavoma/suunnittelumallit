/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

/**
 *
 * @author crazm_000
 */
// On System B
class ProxyImage implements Image {

    private final String filename;
    private final String name;
    private RealImage image;

    /**
     * Constructor
     *
     * @param filename
     */
    public ProxyImage(String filename, String name) {
        this.filename = filename;
        this.name = name;
    }

    /**
     * Displays the image
     */
    public void displayImage() {
        if (image == null) {
            image = new RealImage(filename, name);
        }
        image.displayImage();
    }

    @Override
    public void showData() {

        if (image == null) {
            System.out.println("(Proxy)Name: " + name);
        } else {
            image.showData();
        }

    }
}
