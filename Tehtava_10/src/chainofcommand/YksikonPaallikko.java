/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofcommand;

/**
 *
 * @author crazm_000
 */
public class YksikonPaallikko extends PalkanKorotusHandler{
    private final double maxKorotus = 1.05;
    private final double minKorotus = 1.02;
    
    public YksikonPaallikko(){
        this.titteli="Yksikön päällikkö";
    }
    
    public void kasittele(PalkanKorotusPyynto pyynto) {
        ilmoitaKasittely();
        if(pyynto.getKorotus() >= minKorotus && maxKorotus >= pyynto.getKorotus() ){
            ilmoitaHyvaksyminen();
        }
        else if(seuraava != null){
            ilmoitaSiirto();
            seuraava.kasittele(pyynto);
        }
    }
}
