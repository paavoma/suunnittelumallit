/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofcommand;

/**
 *
 * @author crazm_000
 */
class PalkanKorotusPyynto {

    private double korotus;
    
    private String nimi;

    public PalkanKorotusPyynto(String nimi, double korotus ) {
        double prosentti =  (korotus*100-100);
        System.out.println("Luodaan uusi pyyntö:\n" + "Nimi: " + nimi + "\n" + "Toive: " + prosentti + "%" );
        this.nimi = nimi;
        this.korotus = korotus;
       
    }

  

    public double getKorotus() {
        return korotus;
    }

    public void setKorotus(double korotus) {
        this.korotus = korotus;
    }

    public String getNimi() {
        return nimi;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

}
