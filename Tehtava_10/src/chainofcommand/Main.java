/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofcommand;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ToimitusJohtaja toimitusjohtaja = new ToimitusJohtaja();
        YksikonPaallikko yksikonPaallikko = new YksikonPaallikko();
        Lahiesimies lahiesimies = new Lahiesimies();
        
        lahiesimies.setSeuraava(yksikonPaallikko);
        yksikonPaallikko.setSeuraava(toimitusjohtaja);
        //Palkankorotuspyyntö annetaan muodossa, hakija ja korotus douoblena 1.05=5%
        PalkanKorotusPyynto korotusMatti = new PalkanKorotusPyynto("Matti Hyvaveli", 1.06);
        
        lahiesimies.kasittele(korotusMatti);
        
        PalkanKorotusPyynto korotusEnska = new PalkanKorotusPyynto("Enska Einiinhyvaveli", 1.06);
        lahiesimies.kasittele(korotusEnska);
        
        PalkanKorotusPyynto korotusJantteri = new PalkanKorotusPyynto("Jantteri", 1.019);
        lahiesimies.kasittele(korotusJantteri);
        
        PalkanKorotusPyynto korotusEinari = new PalkanKorotusPyynto("Einari", 1.03);
        lahiesimies.kasittele(korotusEinari);
    }
    
}
