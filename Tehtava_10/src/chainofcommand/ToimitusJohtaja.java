/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofcommand;

/**
 *
 * @author crazm_000
 */
public class ToimitusJohtaja extends PalkanKorotusHandler {
    
    
    
    public ToimitusJohtaja(){
        this.titteli="Toimitusjohtaja";
    }
    
    public void kasittele(PalkanKorotusPyynto pyynto) {
        ilmoitaKasittely();
        if(pyynto.getNimi().equals("Matti Hyvaveli") ){
            ilmoitaHyvaksyminen();
        }else{
            ilmoitaHylkays();
        }
    }
    
   
}
