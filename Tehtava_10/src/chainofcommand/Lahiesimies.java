/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofcommand;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crazm_000
 */
public class Lahiesimies extends PalkanKorotusHandler {

    private final double maxKorotus = 1.02;

    public Lahiesimies() {
        this.titteli = "Lahiesimies";
    }

    public void kasittele(PalkanKorotusPyynto pyynto) {
        ilmoitaKasittely();
        if (maxKorotus >= pyynto.getKorotus()) {
            ilmoitaHyvaksyminen();
        } else if (seuraava != null) {
            ilmoitaSiirto();
            seuraava.kasittele(pyynto);
        }
    }
}
