/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chainofcommand;

/**
 *
 * @author crazm_000
 */
public abstract class PalkanKorotusHandler {

    protected PalkanKorotusHandler seuraava;
    protected double minKorotus;
    protected double maxKorotus;
    protected String titteli;

    public void setSeuraava(PalkanKorotusHandler s) {
        seuraava = s;
    }
    public void kasittele(PalkanKorotusPyynto pyynto) {}

    public void ilmoitaKasittely() {
        System.out.println(this.titteli + " ottaa palkankorotushakemuksen käsittelyyn");
    }

    public void ilmoitaSiirto() {
        System.out.println(this.titteli + " siirtää hakemuksen seuraavalle");
    }

    public void ilmoitaHyvaksyminen() {
        System.out.println(this.titteli + " myöntää palkankorotuksen");
        System.out.println("---------------------------------");
    }
    
    public void ilmoitaHylkays(){
        System.out.println(this.titteli + " ei myönnä palkankorotusta");
        System.out.println("---------------------------------");
    }

}
