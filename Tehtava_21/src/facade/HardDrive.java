/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Paavo
 */
class HardDrive {

    public String read(long lba, int size) {
        String a = Long.toString(lba);
        String b = Integer.toString(size);
        String ab= a+b;
        System.out.println("HD loaded data from: " + lba);
        return ab;
    }
}
