/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Paavo
 */
class Memory {
    String data;
    public void load(long position, String data) {
        this.data=data;
        System.out.println("Data " +data+ " has been loaded to memory from position: " + position);
    }
}
