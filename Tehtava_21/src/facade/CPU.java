/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Paavo
 */
public class CPU {
    public void freeze() {
        System.out.println("Freezing CPU");
    }
    public void jump(long position) {
        System.out.println("CPU moving to position: " + position);
    }
    public void execute() {
        System.out.println("CPU Executing");
    }
}
