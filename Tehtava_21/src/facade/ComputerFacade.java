/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

/**
 *
 * @author Paavo
 */
class ComputerFacade {
    private final CPU processor;
    private final Memory ram;
    private final HardDrive hd;

    public ComputerFacade() {
        this.processor = new CPU();
        this.ram = new Memory();
        this.hd = new HardDrive();
    }

    public void start() {
        processor.freeze();
        ram.load(Address.BOOT_ADDRESS, hd.read(Address.BOOT_SECTOR, Address.SECTOR_SIZE));
        processor.jump(Address.BOOT_ADDRESS);
        processor.execute();
    }
}
