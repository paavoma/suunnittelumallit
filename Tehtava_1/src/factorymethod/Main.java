package factorymethod;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList<AterioivaOtus> otukset = new ArrayList<AterioivaOtus>();
        otukset.add(new Opettaja());
        otukset.add(new Elintenluovutuskoordinaattori());
        otukset.add(new Pastori());     
        
        for(AterioivaOtus otus : otukset) {
        otus.aterioi();
        }
        
    }
}
