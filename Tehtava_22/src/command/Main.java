/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package command;

/**
 *
 * @author Paavo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Light lamp = new Light();
        Screen screen = new Screen();
        Command switchUp = new FlipUpCommand(lamp);
        Command switchDown = new FlipDownCommand(lamp);
        Command upScreen = new ScreenUpCommand(screen);
        Command downScreen = new ScreenDownCommand(screen);
        WallButton button1 = new WallButton(switchUp);
        WallButton button2 = new WallButton(switchDown);
        WallButton button3 = new WallButton(upScreen);
        WallButton button4 = new WallButton(downScreen);
        button1.push();
        button2.push();
        
        button3.push();
        button4.push();
    }
}
