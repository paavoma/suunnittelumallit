/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 *
 * @author crazm_000
 */
public class Pelaaja {

    private Object memento;
    private ArrayList<Integer> arvaamatta = new ArrayList<Integer>();

    public Pelaaja() {
        for (int i = 1; i <= 10; i++) {
            arvaamatta.add(i);
        }
    }

    public void saveMemento(Arvuuttaja arv) {
        this.memento = arv.liityPeliin(this);
    }

    public void palautaMemento(Arvuuttaja arv) {
        arv.tarkistaLuku(memento);

    }

    public int arvaa() {
        
        System.out.println("Arvaamatta olevat: ");
        for (int number : arvaamatta) {
            
            System.out.print(number + ", ");
        }
        
        
        Collections.shuffle(arvaamatta, new Random()); 
        int i = arvaamatta.get(0);
        arvaamatta.remove(0);
        return i;
    }

}
