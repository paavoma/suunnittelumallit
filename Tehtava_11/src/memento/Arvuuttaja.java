/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memento;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author crazm_000
 */
public class Arvuuttaja extends Game {

    int random;
    private int luettuLuku;
    private ArrayList<Pelaaja> players;
    private Scanner input;
    private Pelaaja currentPlayer;
    private int arvattuLuku;
    
    

    @Override
    void initializeGame() {
        arvattuLuku=0;
        luettuLuku=-1;
        players = new ArrayList<Pelaaja>();
        //Alustetaan pelaajat ja annetaan osallistujille mementoihin numerot
        for (int i = 0; i < this.playersCount; i++) {
            players.add(new Pelaaja());

        }
        for (Pelaaja player : players) {
            player.saveMemento(this);

        }

    }

    @Override
    void makePlay(int player) {
        arvattuLuku=0;
        luettuLuku=-1;
        currentPlayer=players.get(player);
        System.out.println("Pelaaja " + player + " arvaa luku?");
        
        //int i = (int) (10.0 * Math.random() + 1);
        int i = currentPlayer.arvaa();
        System.out.println(i);
        arvattuLuku =i;
        
        currentPlayer.palautaMemento(this);
        
        
        
        
    }

    @Override
    boolean endOfGame() {
        if(luettuLuku == arvattuLuku){
            return true;
        }else{
            return false;
        }
    }

    @Override
    void printWinner() {
        System.out.println("We have a winner!!");
    }

    public Object liityPeliin(Pelaaja pelaaja) {
        int random = (int) (10.0 * Math.random() + 1);
        return new Memento(random, pelaaja);
    }
    
    public void tarkistaLuku(Object obj){
        Memento memento=(Memento) obj;
        this.luettuLuku=memento.random;
        System.out.println("Mementossa oleva: " + luettuLuku);
       
    }

    

    private class Memento {
        private Pelaaja pelaaja;
        private int random;

        public Memento(int random, Pelaaja pelaaja) {
            this.random = random;
            this.pelaaja=pelaaja;
        }

    }

}
