/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crazm_000
 */
public class BasicFilewriter implements IFilewriter {

    @Override
    public void writeFile(String stringToWrite) throws IOException {

        FileWriter file = new FileWriter("src/decorator/file.txt");
        BufferedWriter bw = new BufferedWriter(file);

        bw.write(stringToWrite);
        //System.out.println("Tulostetaan kirjoitettu teksti: " + stringToWrite);
        bw.close();
    }

    @Override
    public String readFile() {
        try {
            FileInputStream fis = new FileInputStream("src/decorator/file.txt");
            int i = 0;
            String fileRead = "";

            while ((i = fis.read()) != -1) {
                char a = ((char) i);

                fileRead = fileRead + String.valueOf(a);
            }
            System.out.println("Tulostetaan tiedostosta luettu teksti: " + fileRead);
            return fileRead;

        } catch (FileNotFoundException ex) {
            Logger.getLogger(BasicFilewriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BasicFilewriter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public void printFile(String stringToPrint) {
        System.out.println(stringToPrint);
    }

}
