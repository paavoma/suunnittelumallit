/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.io.IOException;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String teksti = "Olen tallennettava viesti";
        
        System.out.println("Tiedostonkirjoitus ilman salakirjoitusta: " + teksti);
        
        BasicFilewriter writer = new BasicFilewriter();
        writer.writeFile(teksti);
        writer.printFile(writer.readFile());
        
        System.out.println("-------------");
        System.out.println("Tiedostonkirjoitus salakirjoituksella: " + teksti);
        
        IFilewriter encwriter = new FilewriterCipherDecorator(new BasicFilewriter());
        encwriter.writeFile(teksti);
        encwriter.printFile(encwriter.readFile());
        
        
    }
    
}
