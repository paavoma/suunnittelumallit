/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.io.IOException;

/**
 *
 * @author crazm_000
 */
public abstract class FilewriterDecorator implements IFilewriter {

    protected IFilewriter filewriterToDecorate;

    public FilewriterDecorator(IFilewriter filewriterToDecorate) {
        this.filewriterToDecorate = filewriterToDecorate;
    }

    @Override
    public void writeFile(String stringToWrite) throws IOException {
        filewriterToDecorate.writeFile(stringToWrite);
    }
    
    public String readFile(){
       return filewriterToDecorate.readFile();
    }
    public void printFile(String string){
        filewriterToDecorate.printFile(string);
    }
    
}
