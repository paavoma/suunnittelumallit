/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 *
 * @author crazm_000
 */
public class FilewriterCipherDecorator extends FilewriterDecorator {

    public FilewriterCipherDecorator(IFilewriter filewriterToDecorate) {
        super(filewriterToDecorate);
    }

    @Override
    public void writeFile(String stringToWrite) throws IOException {

        super.writeFile(encryptString(stringToWrite));
    }

    private String encryptString(String stringToEncrypt) {
        Base64.Encoder enc = Base64.getEncoder();
        String str = stringToEncrypt;

        // encode with BASE64
        String encoded = enc.encodeToString(str.getBytes());
        return encoded;
    }

    @Override
    public String readFile() {

        return super.readFile();
    }

    @Override
    public void printFile(String string) {

        super.printFile(decodeString(string));
    }

    private String decodeString(String encodedString) {
        // decode 
        Base64.Decoder dec = Base64.getDecoder();
        String decoded = new String(dec.decode(encodedString));
        return decoded;
    }

}
