/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decorator;

import java.io.IOException;

/**
 *
 * @author crazm_000
 */
public interface IFilewriter {
    public void writeFile(String stringToWrite) throws IOException;
    public String readFile();
    public void printFile(String stringToPrint);
    
    
}
