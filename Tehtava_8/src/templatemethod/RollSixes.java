/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package templatemethod;

import static java.lang.Math.random;
import java.util.ArrayList;

/**
 *
 * @author crazm_000
 */
public class RollSixes extends Game {

    int diceOne;
    int diceTwo;
    int currentPlayer;
    ArrayList<Integer> scoreboard = null;

    @Override
    void initializeGame() {
        
        //alustetaan scoreboard ja nopat
        diceOne = 0;
        diceTwo = 0;
        scoreboard = new ArrayList<>();
        for (int i = 0; i < this.playersCount; i++) {
            scoreboard.add(0);
        }

    }

    @Override
    void makePlay(int player) {
        //pelaaja heittää kahta noppaa, tuplakuutoset antaa pisteen
        currentPlayer = player + 1;

        diceOne = (int) (6.0 * Math.random() + 1);
        diceTwo = (int) (6.0 * Math.random() + 1);
        System.out.println("Player " + currentPlayer + " rolls " + diceOne + " and " + diceTwo);
        if (diceOne == 6 && diceTwo == 6) {
            scoreboard.set(player, (scoreboard.get(player)+1));
            System.out.println("Player " + currentPlayer + " gets a point and has " + scoreboard.get(player) + " point(s)");
            

            
        }

    }

    @Override
    boolean endOfGame() {
        //se ken saa ensimmäisenä 5 pistettä, voittaa
        if (scoreboard.contains(5)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    void printWinner() {
        
        //tulostetaan voittaja ja kaikkien pisteet
        System.out.println("The winner is player " + currentPlayer);
        int playernumber = 0;
        for(int i : scoreboard){
            playernumber++;
            System.out.println("Player " + playernumber + ": "+ i);
            
        }
    }

}
