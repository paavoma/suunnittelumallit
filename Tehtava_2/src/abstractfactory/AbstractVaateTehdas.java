/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author crazm_000
 */
public interface AbstractVaateTehdas {

    public abstract Farmarit createFarmarit();

    public abstract Kengat createKengat();

    public abstract Lippis createLippis();

    public abstract Tpaita createTpaita();

}
