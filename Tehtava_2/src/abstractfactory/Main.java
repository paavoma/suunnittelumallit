package abstractfactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {

        Class c = null;
        AbstractVaateTehdas tehdas = null;
        Properties properties = new Properties();
        
        //asetetaan tehdas adidastehtaaksi
        try {
            properties.load(
                    new FileInputStream("src/abstractfactory/tehdas.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {

            c = Class.forName(properties.getProperty("tehdasAdidas"));
            tehdas = (AbstractVaateTehdas) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //AbstractVaateTehdas tehdas = new AdidasTehdas();
        Jasper jamppa = new Jasper(tehdas);
        jamppa.vaateta();
        jamppa.luettele();
        
        
        //vaihdetaan tehdas bossin tehtaaseen
        try {
            properties.load(
                    new FileInputStream("src/abstractfactory/tehdas.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {

            c = Class.forName(properties.getProperty("tehdasBoss"));
            tehdas = (AbstractVaateTehdas) c.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        jamppa = new Jasper(tehdas);
        jamppa.vaateta();
        jamppa.luettele();

    }
}
