/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

/**
 *
 * @author crazm_000
 */
public class AdidasTehdas implements AbstractVaateTehdas {

    @Override
    public Farmarit createFarmarit() {
        return new AdidasFarmarit();
    }

    @Override
    public Kengat createKengat() {
        return new AdidasKengat();
    }

    @Override
    public Lippis createLippis() {
        return new AdidasLippis();
    }

    @Override
    public Tpaita createTpaita() {
        return new AdidasTpaita();
    }

}
