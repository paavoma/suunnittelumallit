/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;


public class Jasper {

    private AbstractVaateTehdas tehdas;
    private Farmarit farmarit = null;
    private Kengat kengat = null;
    private Lippis lippis = null;
    private Tpaita tpaita = null;

    public Jasper(AbstractVaateTehdas tehdas) {
        this.tehdas = tehdas;
    }

    public void vaateta() {
        farmarit = tehdas.createFarmarit();
        kengat = tehdas.createKengat();
        lippis = tehdas.createLippis();
        tpaita = tehdas.createTpaita();
    }

    public void luettele() {
        System.out.println("Päälläni on:");
        System.out.println(farmarit);
        System.out.println(kengat);
        System.out.println(lippis);
        System.out.println(tpaita);

    }

}
