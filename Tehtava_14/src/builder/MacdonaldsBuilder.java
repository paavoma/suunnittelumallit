/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author crazm_000
 */
public class MacdonaldsBuilder extends HamburgerBuilder{
    public void buildBun() {
        hamburger.setoBun(new Bun());
    }

    public void buildPatty() {
        hamburger.setoPatty(new Patty());
    }

    public void buildLettuce() {
        hamburger.setoLettuce(new Lettuce());
    }
}
