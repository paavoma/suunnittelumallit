/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author crazm_000
 */
public class HesburgerBuilder extends HamburgerBuilder {

    public void buildBun() {
        hamburger.addToBurger("Hesburger bun \n");
    }

    public void buildPatty() {
        hamburger.addToBurger("Hesburger patty \n");
    }

    public void buildLettuce() {
        hamburger.addToBurger("Hesburger lettuce \n");
    }
    
}
