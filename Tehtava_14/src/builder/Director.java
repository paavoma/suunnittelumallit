/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author crazm_000
 */
public class Director {

    private HamburgerBuilder burgerBuilder;

    public void setHamburgerBuilder(HamburgerBuilder hb) {
        burgerBuilder = hb;
    }

    public Hamburger getHamburger() {
        return burgerBuilder.getHamburger();
    }

    public void constructHamburger() {
        burgerBuilder.createNewHamburger();
        burgerBuilder.buildBun();
        burgerBuilder.buildPatty();
        burgerBuilder.buildLettuce();
    }

}
