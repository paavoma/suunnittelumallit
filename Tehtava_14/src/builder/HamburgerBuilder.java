/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author crazm_000
 */
public abstract class HamburgerBuilder {
   protected Hamburger hamburger;
   
   public Hamburger getHamburger(){
       return hamburger;
   }
   public void createNewHamburger(){
       hamburger = new Hamburger();
   }
   
   public abstract void buildBun();
   public abstract void buildPatty();
   public abstract void buildLettuce();

    
   
   
}
