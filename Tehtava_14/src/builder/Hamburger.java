/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import java.util.ArrayList;

/**
 *
 * @author crazm_000
 */
public class Hamburger {

    private StringBuilder hamburger;
    private Bun oBun;
    private Patty oPatty;
    private Lettuce oLettuce;
    private ArrayList<BurgerIngredient> content = new ArrayList<>();

    public Hamburger() {
        
    }

    public void addToBurger(String ingredient) {
        if(hamburger == null){
            hamburger=new StringBuilder();
        }
        hamburger.append(ingredient);
    }

    public void printContent() {
        //tulostetaan stringbuilderiin osista rakennettu hampurilainen
        System.out.println("This hamburger contains: ");
        if (hamburger != null) {
            System.out.println(hamburger.toString());
        }
        //oliot taulukkoon ja tulostetaan hampurilaisen osien nimet
        content.add(oBun);
        content.add(oPatty);
        content.add(oLettuce);
        
        for (BurgerIngredient bi : content) {
            if (bi != null) {
                System.out.println(bi.getName());
            }
        }

    }

    public Bun getoBun() {
        return oBun;
    }

    public void setoBun(Bun oBun) {
        this.oBun = oBun;
    }

    public Patty getoPatty() {
        return oPatty;
    }

    public void setoPatty(Patty oPatty) {
        this.oPatty = oPatty;
    }

    public Lettuce getoLettuce() {
        return oLettuce;
    }

    public void setoLettuce(Lettuce oLettuce) {
        this.oLettuce = oLettuce;
    }

}
