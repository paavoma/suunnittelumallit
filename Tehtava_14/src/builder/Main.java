/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //luodaan tirehtööri ja hampurilaisrakentajat
        Director director = new Director();
        HamburgerBuilder mac= new MacdonaldsBuilder();
        HamburgerBuilder hese= new HesburgerBuilder();
        
        //asetetaan tirehtöörille hampurilaisrakentaja, ja tehdään hampurilainen
        director.setHamburgerBuilder(mac);
        director.constructHamburger();
        Hamburger hamburgerMac=director.getHamburger();
        hamburgerMac.printContent();
        
        //vaihdetaan rakentaja
        director.setHamburgerBuilder(hese);
        director.constructHamburger();
        Hamburger hamburgerHese=director.getHamburger();
        hamburgerHese.printContent();
    }
    
}
