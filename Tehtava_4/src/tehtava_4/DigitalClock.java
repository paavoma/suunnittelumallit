/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava_4;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author crazm_000
 */
public class DigitalClock implements Observer{
    private ClockTimer timer;
    
    public DigitalClock(ClockTimer ct){
        timer = ct;
        timer.addObserver(this);
    }
    

    private void draw() {
       int hours = timer.getHour();
       int mins = timer.getMinute();
       int secs = timer.getSeconds();
       
        System.out.println(hours + " h " + mins + " min " + secs + " sec");
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o == timer){
            draw();
        }
    }
    
    
}
