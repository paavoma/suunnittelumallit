/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtava_4;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author crazm_000
 */
public class ClockTimer extends Observable {

    private int hours = 0;
    private int mins = 0;
    private int seconds = 0;

    public int getHour() {
        return hours;
    }

    public int getMinute() {
        return mins;
    }

    public int getSeconds() {
        return seconds;
    }

    public void tick() {

        seconds++;

        if (seconds > 59) {
            seconds = 0;
            mins++;
        }
        if (mins > 59) {
            mins = 0;
            hours++;
        }
        if (hours > 23) {
            hours = 0;
        }
        setChanged();
        notifyObservers();
    }

    //Mallintaa yhden sekunnin kulumista, nopealla, jotta voi testata sovelluksen toimivan oikein.
    public void startClock(int interval) {
        int countdown = 500000;
        while (countdown > 0) {
            tick();
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClockTimer.class.getName()).log(Level.SEVERE, null, ex);
            }
            countdown--;
        }
    }

}
