/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

/**
 *
 * @author crazm_000
 */
public class ComponentFactoryAsus implements ComponentFactory{
    
    
    public ComputerPart createNetworkCard() {
        return new AsusNetworkCard();
    }

    
    public ComputerPart createMotherboard() {
        return new AsusMotherboard();
    }

    
    public ComputerPart createComputer() {
        return new Computer();
    }

    
    public ComputerPart createMotherboard(ComponentFactory factory) {
        return new AsusMotherboard(factory);
    }

    @Override
    public ComputerPart createPowerSupply() {
        return new AsusPowerSupply();
    }

    @Override
    public ComputerPart createCase(ComponentFactory factory) {
        return new AsusCase(factory);
    }

    @Override
    public ComputerPart createMemory() {
        return new AsusMemory();
    }

    @Override
    public ComputerPart createGraphicsCard() {
        return new AsusGraphicsCard();
    }

    @Override
    public ComputerPart createProcessor() {
        return new AsusProcessor();
    }
}
