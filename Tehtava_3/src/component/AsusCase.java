/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

import java.util.ArrayList;

/**
 *
 * @author crazm_000
 */
public class AsusCase implements ComputerPart {

    private ComponentFactory factory;
    private final double price = 60;
    private final String name = "Asus Case";
    private ArrayList<ComputerPart> partList = new ArrayList<ComputerPart>();
    
    //Emolevyyn kuuluu myös verkkokortti
    public AsusCase(ComponentFactory factory){
        this.factory=factory;
        addComputerPart(factory.createPowerSupply());
    }

    public AsusCase() {
       
    }
    
    public void addComputerPart(ComputerPart part){
        partList.add(part);
    }

 
    public double getPrice() {
       double totalPrice =price;
       for (ComputerPart p : partList){
           totalPrice= totalPrice+p.getPrice();
       }
       return totalPrice;
    }


    public String getName() {
       return name;
    }
    
    
    public void printProduct(){
        
        System.out.println(getName() + " " + price);
        for (ComputerPart p : partList){
            System.out.println("inc. " + p.getName() + " " + p.getPrice());;
       }
        
        
    }
}
