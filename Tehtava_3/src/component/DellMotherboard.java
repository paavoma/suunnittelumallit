/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

import java.util.ArrayList;


public class DellMotherboard implements ComputerPart{
    private ComponentFactory factory;
    private final double price = 130;
    private final String name = "Dell Motherboard";
    private ArrayList<ComputerPart> partList = new ArrayList<ComputerPart>();
    
    //Emolevyyn kuuluu myös verkkokortti
    public DellMotherboard(ComponentFactory factory){
        this.factory=factory;
        addComputerPart(factory.createNetworkCard());
    }

    public DellMotherboard() {
       
    }
    
    public void addComputerPart(ComputerPart part){
        partList.add(part);
    }

 
    public double getPrice() {
       double totalPrice =price;
       for (ComputerPart p : partList){
           totalPrice= totalPrice+p.getPrice();
       }
       return totalPrice;
    }


    public String getName() {
       return name;
    }
    
    
    public void printProduct(){
        
        System.out.println(getName() + " " + price);
        for (ComputerPart p : partList){
            System.out.println("inc. " + p.getName() + " " + p.getPrice());;
       }
        
        
    }
    
}
