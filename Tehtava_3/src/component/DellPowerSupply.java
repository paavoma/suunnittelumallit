/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

/**
 *
 * @author crazm_000
 */
public class DellPowerSupply implements ComputerPart{
    private final String name ="Dell Power Supply";
    private final double price=90;
    
    public DellPowerSupply(){
       
    }

 
    @Override
    public double getPrice() {
        return price;
    }

 
    @Override
    public String getName() {
        return name;
    }

    
    @Override
    public void addComputerPart(ComputerPart part) {
        throw new RuntimeException("Cannot add parts to a part");
    }

    @Override
    public void printProduct() {
        System.out.println(getName() + " " + getPrice());
    }
    
}
