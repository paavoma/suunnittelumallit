/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

/**
 *
 * @author crazm_000
 */
public interface ComponentFactory {
    
    public ComputerPart createNetworkCard();
    public ComputerPart createMotherboard(ComponentFactory factory);
    public ComputerPart createComputer();
    public ComputerPart createPowerSupply();
    public ComputerPart createCase(ComponentFactory factory);
    public ComputerPart createMemory();
    public ComputerPart createGraphicsCard();
    public ComputerPart createProcessor();
    
}
