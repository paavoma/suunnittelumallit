/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

/**
 *
 * @author crazm_000
 */
public interface ComputerPart {
    
    
    public double getPrice();
    public String getName();
    public void addComputerPart(ComputerPart part);
    public void printProduct();
    
}
