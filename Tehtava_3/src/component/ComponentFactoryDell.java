/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

/**
 *
 * @author crazm_000
 */
public class ComponentFactoryDell implements ComponentFactory{
    
    
    public ComputerPart createNetworkCard() {
        return new DellNetworkCard();
    }

    
    public ComputerPart createMotherboard() {
        return new DellMotherboard();
    }

    
    public ComputerPart createComputer() {
        return new Computer();
    }

    
    public ComputerPart createMotherboard(ComponentFactory factory) {
        return new DellMotherboard(factory);
    }

    @Override
    public ComputerPart createPowerSupply() {
        return new DellPowerSupply();
    }

    @Override
    public ComputerPart createCase(ComponentFactory factory) {
        return new DellCase(factory);
    }

    @Override
    public ComputerPart createMemory() {
        return new DellMemory();
    }

    @Override
    public ComputerPart createGraphicsCard() {
        return new DellGraphicsCard();
    }

    @Override
    public ComputerPart createProcessor() {
        return new DellProcessor();
    }
}
