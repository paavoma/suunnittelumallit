/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

import java.util.ArrayList;

/**
 *
 * @author crazm_000
 */
public class Computer implements ComputerPart {

    private ArrayList<ComputerPart> partList = new ArrayList<ComputerPart>();

    public Computer() {

    }

    //palauttaa summan koko koneen hinnasta
    @Override
    public double getPrice() {
        double totalprice = 0;
        for (ComputerPart part : partList) {
            totalprice = totalprice + part.getPrice();
        }
        return totalprice;
    }

    public void printNames() {
        for (ComputerPart part : partList) {
            System.out.println(part.getName());
        }
    }

    @Override
    public void addComputerPart(ComputerPart part) {
        partList.add(part);
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void printProduct() {
        int n = 0;
        System.out.println("Computer parts: ");
        for (ComputerPart part : partList) {
            n++;
            System.out.print(n + ". ");
            part.printProduct();
        }
        System.out.println(getPrice());
    }

}
