/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package component;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Valitaan asuksen tehdas
        ComponentFactory factory = new ComponentFactoryAsus();
        ComputerPart computer = factory.createComputer();
        
        computer.addComputerPart(factory.createMotherboard(factory));
        computer.addComputerPart(factory.createCase(factory));
        computer.addComputerPart(factory.createMemory());
        computer.addComputerPart(factory.createPowerSupply());
        computer.addComputerPart(factory.createGraphicsCard());
        computer.addComputerPart(factory.createProcessor());
       
        computer.printProduct();
        
        //Valitaan Dellin tehdas
        factory = new ComponentFactoryDell();
        computer = factory.createComputer();
        
        computer.addComputerPart(factory.createMotherboard(factory));
        computer.addComputerPart(factory.createCase(factory));
        computer.addComputerPart(factory.createMemory());
        computer.addComputerPart(factory.createPowerSupply());
        computer.addComputerPart(factory.createGraphicsCard());
        computer.addComputerPart(factory.createProcessor());
        computer.printProduct();
        
        
        
    }
    
}
