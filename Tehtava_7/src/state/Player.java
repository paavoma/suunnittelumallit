/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author crazm_000
 */
public class Player {

    private EvolveState evolveState;

    public Player() {
        evolveState = CharmanderState.getInstance();
    }

    protected void changeState(EvolveState e) {
        evolveState = e;
    }

    public void commandAttack() {
        evolveState.attack(this);
    }
    public void commandEat() {
        evolveState.eat(this);
    }

    public void commandEvolve() {
        evolveState.evolve(this);
    }
    public void commandJump(){
        evolveState.jump(this);
    }

}
