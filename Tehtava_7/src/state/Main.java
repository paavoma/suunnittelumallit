/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Player player = new Player();
        //Pistetään eri statet tekemään samat temput, Evolve vaihtaa statea toiseen
        player.commandAttack();
        player.commandJump();
        player.commandEvolve();
        player.commandAttack();
        player.commandJump();
        player.commandEvolve();
        player.commandAttack();
        player.commandJump();
        player.commandEvolve();
    }
    
}
