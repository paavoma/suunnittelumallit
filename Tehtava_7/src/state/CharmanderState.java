/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package state;

/**
 *
 * @author crazm_000
 */
public class CharmanderState extends EvolveState {

    private static CharmanderState singleton;

    private CharmanderState() {

        this.setName("Charmander");

    }

    public static synchronized CharmanderState getInstance() {
        if (singleton == null) {
            singleton = new CharmanderState();
        }
        return singleton;
    }

    void attack(Player p) {

        System.out.println(this.getName() + " attacks with claws");
    }
    
    void jump(Player p){
        System.out.println(this.getName() + " jumps");
    }

    void evolve(Player p) {

        System.out.println(this.name + " tries to evolve");
        p.changeState(CharmeleonState.getInstance(this.name));
    }

}
