/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.util.Random;

/**
 *
 * @author crazm_000
 */
public class ModelData {

    private int[] array;

   
    private int maxRandom;
    private int amountToSort;

    public ModelData(int amount, int max) {
        generateArray(amount, max);
        this.maxRandom=max;
        this.amountToSort=amount;
    }

    //Luo taulukon jossa amount määrä alkoioita välillä 1-max
    public ModelData generateArray(int amount, int max) {
        array = new int[amount + 1];
        this.maxRandom = max;
        this.amountToSort = amount;

        Random random = new Random();

        for (int i = 0; i <= amount; i++) {
            array[i] = random.ints(1, (max + 1)).findFirst().getAsInt();

        }
        return this;
    }
    
    public int[] getArray(){
        return array;
    }
    
    public void printArray() {
      
        for (int i = 0; i < this.array.length - 1; i++) {
            System.out.print(this.array[i] + "\t");
            if((i + 1) % 10 == 0) System.out.println("\\");
        }
    }
     public int getMaxRandom() {
        return maxRandom;
    }

    public int getAmountToSort() {
        return amountToSort;
    }
}
