
package strategy;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        //generoidaan 100000 alkiota int lukuja väliltä 1-1000 ja sortataan ne
        ModelData data=new ModelData(1000000, 1000);
        Context context= new Context(new SelectionSortStrategy(), data);
        //context.doSort(data);
   
        
        
        //vaihdetaan algoritmi mergesortiin
        context.setStrategy(new MergeSortStrategy());
        context.doSort(data);
        
        //vaihdetaan algoritmi quicksortiin
        context.setStrategy(new QuickSortStrategy());
        context.doSort(data);
        
        
        
        
        
    }
    
}
