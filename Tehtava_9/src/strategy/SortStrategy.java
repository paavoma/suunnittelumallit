/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

/**
 *
 * @author crazm_000
 */
public interface SortStrategy {
    public int[] sort(int[] inputArray);
    public String getName();
}
