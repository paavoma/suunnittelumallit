/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

/**
 *
 * @author crazm_000
 */
public class SelectionSortStrategy implements SortStrategy {
    private int[] sortedArray;
    private String name = "SelectionSort ";

    
    
    
    public SelectionSortStrategy(){
        sortedArray=null;
    }
    public int[] sort(int[] inputArray){
       
       sortedArray = doSelectionSort(inputArray);
       return sortedArray;
    }
    
    public static int[] doSelectionSort(int[] arr){
         
        for (int i = 0; i < arr.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index]) 
                    index = j;
      
            int smallerNumber = arr[index];  
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        
        return arr;
    }
    public String getName() {
        return name;
    }
   
    
}
