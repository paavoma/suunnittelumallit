/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strategy;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author crazm_000
 */

//Generoi tietueen, säilyttää, lähettää strategylle prosessoitavaksi ja tulostaa
public class Context {

    private SortStrategy strategy;
    private final int[] array;
    private int[] arrangedArray;
    private Timestamp timestamp;
    
    

    public Context(SortStrategy strategy, ModelData data) {
        this.strategy = strategy;
        this.array=data.getArray();
        this.arrangedArray=null;
    }
    //käskyttää nykyistä strategya tekemään sorttauksen ja tulostaa järjestelyyn kuluneen ajan;
    public void doSort(ModelData data) {
        timestamp=null;
        System.out.println("------------------------------------------------------");
        System.out.println(strategy.getName() + "with " + data.getAmountToSort() + "random integer numbers between 1 and " + data.getMaxRandom());
        //ottaa timestampin ennen ja jälkeen
        System.out.println("Unsorted: ");
        //printArray(array);
        
        //kopioidaan uuteen tauluun, jotta sorttaamaton aineisto pysyy koskemattomana
        int[] arrayTo = Arrays.copyOf(array, array.length);           
        
        timestamp = new Timestamp(System.currentTimeMillis());
        int totaltime =(int)timestamp.getTime();
        arrangedArray = strategy.sort(arrayTo); 
        timestamp = new Timestamp(System.currentTimeMillis());
        totaltime = (int)timestamp.getTime()-totaltime ;
        
        System.out.println("Sorted: ");
        //printArray(arrangedArray);
        System.out.println("Sort time: " + totaltime + " ms");

    }

    public void setStrategy(SortStrategy strategy) {
        this.strategy = strategy;
        
    }
    
    
    //tulostaa taulukon
    public void printArray(int[] inputArray) {
      
        for (int i = 0; i < inputArray.length - 1; i++) {
            System.out.print(inputArray[i] + "\t");
            if((i + 1) % 10 == 0) System.out.println("\\");
        }
    }

}
