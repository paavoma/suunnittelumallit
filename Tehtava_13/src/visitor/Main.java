/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

import java.util.ArrayList;

/**
 *
 * @author crazm_000
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Character bulbasaur = new Bulbasaur();
        Character charmander = new Charmander();
        ArrayList<Character> characters = new ArrayList<Character>();
        characters.add(bulbasaur);
        characters.add(charmander);
        
        BonusVisitor visitor = new BonusVisitor();
        //Pistetään eri statet tekemään samat temput, Evolve vaihtaa statea toiseen
        for(Character c : characters){
            c.commandAttack();
        }
        bulbasaur.commandEvolve();
        for(Character c : characters){
            c.grantBonus(visitor);
        }
        bulbasaur.commandEvolve();
        charmander.commandEvolve();
        for(Character c : characters){
            c.grantBonus(visitor);
        }
        
        
        
        
    }
    
}
