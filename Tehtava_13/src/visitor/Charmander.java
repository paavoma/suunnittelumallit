/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public class Charmander extends Character {
    
    
    
   

    public Charmander() {
        points=0;
        evolveState = CharmanderState.getInstance();
    }
    
    
    @Override
    public void commandAttack() {
        evolveState.attack(this);
    }
    
    @Override
    public void commandEat() {
        evolveState.eat(this);
    }

    
    @Override
    public void commandEvolve() {
        evolveState.evolve(this);
    }
   
    @Override
    public void commandJump(){
        evolveState.jump(this);
    }
    
    

}
