/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public abstract class Character {
    protected EvolveState evolveState;
    protected int points;

    protected void changeState(EvolveState e) {
        evolveState = e;
    }
    
    
    public void commandAttack() {
        
        
    }
    
    public void commandEat() {
        
    }

    
    public void commandEvolve() {
       
    }
    
    public void commandJump(){
       
    }
    
    public void grantBonus(BonusVisitor visitor){
        
        evolveState.accept(visitor);
        System.out.println("Granting " +evolveState.getBonus()+ " bonuspoints to " + evolveState.getName());
        points=points+evolveState.getBonus();
        System.out.println(evolveState.getName() + " total points:" + points);
    }
    
}
