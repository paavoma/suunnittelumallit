/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;



interface Visitor{
    
    void visit(CharmanderState cha);
    void visit(CharmeleonState cme);
    void visit(CharizardState chz);
    void visit(BulbasaurState bul);
    void visit(IvysaurState ivy);
    void visit(VenusaurState ven);
    
}
/**
 *
 * @author 
 */
class BonusVisitor implements Visitor{

    @Override
    public void visit(CharmanderState cha) {
        cha.setBonus(1);
    }

    @Override
    public void visit(CharmeleonState cme) {
        cme.setBonus(3);
    }

    @Override
    public void visit(CharizardState chz) {
        chz.setBonus(5);
    }
    
    @Override
    public void visit(BulbasaurState bul) {
        bul.setBonus(2);
    }

    @Override
    public void visit(IvysaurState ivy) {
        ivy.setBonus(4);
    }

    @Override
    public void visit(VenusaurState ven) {
        ven.setBonus(6);
    }
    
    
}
