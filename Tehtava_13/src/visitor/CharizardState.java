/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public class CharizardState extends EvolveState{
    
    private static CharizardState singleton;

    private CharizardState(String prevName) {
        bonus=0;
        this.setName("Charizard");
        System.out.println(prevName + " has evolved into " + this.getName());
    }

    public static synchronized CharizardState getInstance(String name) {
        if (singleton == null) {
            singleton = new CharizardState(name);
        }
        return singleton;
    }
    
    void attack(Charmander p){
        System.out.println(this.getName() + " uses breath attack");
    }
    
    void jump(Charmander p){
        System.out.println(this.getName() + " begins to fly");
    }
    
    
    void evolve(Charmander p){
        System.out.println(this.name + " tries to evolve");
        System.out.println(this.getName() + " cannot evolve beyond this!");
    }
    
    void accept(BonusVisitor visitor){
        visitor.visit(this);
    }
    

    
}
