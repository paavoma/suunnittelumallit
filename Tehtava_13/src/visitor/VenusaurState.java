/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public class VenusaurState extends EvolveState{
    
    private static VenusaurState singleton;

    private VenusaurState(String prevName) {
        bonus=0;
        this.setName("Venusaur");
        System.out.println(prevName + " has evolved into " + this.getName());
    }

    public static synchronized VenusaurState getInstance(String name) {
        if (singleton == null) {
            singleton = new VenusaurState(name);
        }
        return singleton;
    }
    
    void attack(Bulbasaur p){
        System.out.println(this.getName() + " uses toxic splash");
    }
    
    void jump(Bulbasaur p){
        System.out.println(this.getName() + " begins to fly");
    }
    
    
    void evolve(Bulbasaur p){
        System.out.println(this.name + " tries to evolve");
        System.out.println(this.getName() + " cannot evolve beyond this!");
    }
    
    void accept(BonusVisitor visitor){
        visitor.visit(this);
    }
    

    
}
