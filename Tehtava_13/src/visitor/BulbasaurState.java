/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public class BulbasaurState extends EvolveState {

    private static BulbasaurState singleton;

    private BulbasaurState() {
        bonus=0;
        this.setName("Bulbasaur");

    }

    public static synchronized BulbasaurState getInstance() {
        if (singleton == null) {
            singleton = new BulbasaurState();
        }
        return singleton;
    }

    void attack(Bulbasaur p) {

        System.out.println(this.getName() + " attacks with spores");
    }
    
    void jump(Bulbasaur p){
        System.out.println(this.getName() + " jumps");
    }

    void evolve(Bulbasaur p) {

        System.out.println(this.name + " tries to evolve");
        p.changeState(IvysaurState.getInstance(this.name));
    }
    void accept(BonusVisitor visitor){
        visitor.visit(this);
    }

}
