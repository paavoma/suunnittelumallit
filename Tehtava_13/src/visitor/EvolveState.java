/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
abstract class EvolveState {

    protected int bonus;

    
    protected String name;
    

    void attack(Charmander p) {}

    void eat(Charmander p) {}
    
    void jump(Charmander p) {}
    
    void evolve(Charmander p) {}
    
    void attack(Bulbasaur b) {}

    void eat(Bulbasaur b) {}
    
    void jump(Bulbasaur b) {}
    
    void evolve(Bulbasaur b) {}
    
    
    

    public String getName() {
        return this.name;
    }
    
  

    public void setName(String name) {
        this.name = name;
    }

    
    
    void accept(BonusVisitor visitor){
        
    }
    
    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

}
