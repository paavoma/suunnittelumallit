/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public class IvysaurState extends EvolveState {

    private static IvysaurState singleton;

    private IvysaurState(String prevName) {
        bonus=0;
        this.setName("Ivysaur");
        System.out.println(prevName + " has evolved into " + this.getName());
    }

    public static synchronized IvysaurState getInstance(String prevName) {
        if (singleton == null) {
            singleton = new IvysaurState(prevName);
        }
        return singleton;
    }

    void attack(Bulbasaur p) {
        System.out.println(this.getName() + " attacks with poison ivy");
    }
    
    void jump(Bulbasaur p){
        System.out.println(this.getName() + " does a double jump");
    }

    void evolve(Bulbasaur p) {

        System.out.println(this.name + " tries to evolve");
        p.changeState(VenusaurState.getInstance(this.name));
    }
    void accept(BonusVisitor visitor){
        visitor.visit(this);
    }

}
