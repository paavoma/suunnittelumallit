/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package visitor;

/**
 *
 * @author crazm_000
 */
public class CharmeleonState extends EvolveState {

    private static CharmeleonState singleton;

    private CharmeleonState(String prevName) {
        bonus=0;
        this.setName("Charmeleon");
        System.out.println(prevName + " has evolved into " + this.getName());
    }

    public static synchronized CharmeleonState getInstance(String prevName) {
        if (singleton == null) {
            singleton = new CharmeleonState(prevName);
        }
        return singleton;
    }

    void attack(Charmander p) {
        System.out.println(this.getName() + " attacks with tongue");
    }
    
    void jump(Charmander p){
        System.out.println(this.getName() + " does a double jump");
    }

    void evolve(Charmander p) {

        System.out.println(this.name + " tries to evolve");
        p.changeState(CharizardState.getInstance(this.name));
    }
    void accept(BonusVisitor visitor){
        visitor.visit(this);
    }

}
